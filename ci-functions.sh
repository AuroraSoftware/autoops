#!/bin/sh
# Auto DevOps variables and functions
[[ "$TRACE" ]] && set -x
export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG

function registry_login() {
  if [[ -n $DOCKERHUB_USER ]]; then
      echo "Logging to Docker HUB..."
      docker login -u "$DOCKERHUB_USER" -p "$DOCKERHUB_PASSWORD" 
  elif [[ -n "$CI_REGISTRY_USER" ]]; then
        echo "Logging to GitLab Container Registry with CI credentials..."
        docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
        echo ""
  else
      echo "No registry login is enabled, you need to define DOCKERHUB_USER or CI_REGISTRY_USER for gitlab registry"
  fi
}

function notify_slack() {
  if [[ -n $SLACK_TOKEN ]]; then
      curl -s -H "Content-Type:application/json" https://hooks.slack.com/services/${SLACK_TOKEN} -X POST -d "{\"text\": \"${slack_message}\"}"
  else
      echo "No SLACK_TOKEN defined"
  fi    
}

function notify_slack_attachment() {
  if [[ -n $SLACK_TOKEN ]]; then
      curl -s -H "Content-Type:application/json" https://hooks.slack.com/services/${SLACK_TOKEN} -X POST -d "{\"attachments\": [{\"fallback\": \"Required plain-text summary of the attachment.\",\"color\": \"#36a64f\",\"pretext\": \"${slack_message}\",\"author_name\": \"${slack_author:-Anonymous}\",\"text\": \"${slack_attachment}\"}]}"
  else
      echo "No SLACK_TOKEN defined"
  fi
}

function code_quality() {
  docker run --env SOURCE_CODE="$PWD" \
             --volume "$PWD":/code \
             --volume /var/run/docker.sock:/var/run/docker.sock \
             "registry.gitlab.com/gitlab-org/security-products/codequality:$SP_VERSION" /code
}  

function generate_env() {
    cat .env.default > .env || true
    printf "\n" >> .env || true # make sure new line is added between .env.default and the rest
    if [[ $REACT_APP == "true" ]]; then
      set | grep -e ^FLAG_ | while read line; do echo REACT_APP_$line >> .env; done || true
      set | grep -e ^CONFIG_ | while read line; do echo REACT_APP_$line >> .env; done || true
      printf "\n" >> .env || true 
    else
      set | grep -e ^FLAG_ >> .env || true
      set | grep -e ^CONFIG_ >> .env || true
      printf "\n" >> .env || true    
    fi
    if [[ $CI_JOB_STAGE == "test" ]]; then
      cat .env.test >> .env || true
      printf "\n" >> .env || true       
    fi
    cat .env # debug output  
    source .env # load all the env vars into system environment
}

  function build() {
    registry_login
    if [[ -f Dockerfile ]]; then
      echo "Building Dockerfile-based application..."
        if [[ -n $DOCKERHUB_USER ]]; then
         docker build \
           --pull \
           --build-arg env=$CI_COMMIT_REF_NAME \
           --build-arg project_name=$CI_PROJECT_NAME \
           --build-arg satis_pass=$satis_password \
           --build-arg custom_arg1=$custom_arg1 \
           --build-arg custom_arg2=$custom_arg2 \
           --build-arg custom_arg3=$custom_arg3 \
           -t "$DOCKERHUB_USER/$DOCKERHUB_REPO:$CI_COMMIT_SHA" -t "$DOCKERHUB_USER/$DOCKERHUB_REPO:$CI_COMMIT_REF_NAME" .
         docker push "$DOCKERHUB_USER/$DOCKERHUB_REPO:$CI_COMMIT_SHA"
         docker push "$DOCKERHUB_USER/$DOCKERHUB_REPO:$CI_COMMIT_REF_NAME"
        else
         docker build \
           --pull \
           --build-arg env=$CI_COMMIT_REF_NAME \
           --build-arg project_name=$CI_PROJECT_NAME \
           --build-arg satis_pass=$satis_password \
           --build-arg custom_arg1=$custom_arg1 \
           --build-arg custom_arg2=$custom_arg2 \
           --build-arg custom_arg3=$custom_arg3 \
           -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA" -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME" .
         docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
         docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
        fi
    fi    
}

function deploy() {
  if [[ "$CI_DEPLOY_ECS" = "true" ]]; then
      GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" git clone $CI_ANSIBLE_REPO ansible
      cd ansible && echo "$ANSIBLE_VAULT_PASSWORD" > .vault_pass && ansible-playbook aws.yml -e env="${ansible_env:-$CI_COMMIT_REF_NAME}" -e project_name="${PROJECT_NAME:-$CI_PROJECT_NAME}" -e docker_tag="${docker_env:-$CI_COMMIT_REF_NAME}" -e ecs_service_autoscaling="${ECS_AUTOSCALING:-false}" -e reset_db="${reset_db:-false}" --vault-password-file .vault_pass -t ecs -vvvv
  else
      echo "No deploy method was defined"
  fi
}

function ssh-key() {
    if [[ -z "$SSH_PRIVATE_KEY" ]]; then
        echo "SSH_PRIVATE_KEY needs to be defined"; exit 1
    else
        mkdir -p ~/.ssh && chmod 700 ~/.ssh && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
        echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa
    fi
}